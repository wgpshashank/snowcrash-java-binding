# Make snowcrash for testing AST v2

## Prerequisites
Install bundler:

    sudo gem install bundler

## Install
Run the following:

    git clone --recursive git://github.com/apiaryio/snowcrash.git
    cd snowcrash
    git checkout v0.15.0

Update submodules:

    git submodule update --init --recursive

Build:

    ./configure
    make

Finally, copy ``./bin/snowcrash`` here.
