package com.gatehill.apib.parser.service;

import com.gatehill.apib.parser.exception.ParserException;
import com.gatehill.apib.parser.model.AstFormat;
import com.gatehill.apib.parser.model.Blueprint;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Build a {@link com.gatehill.apib.parser.model.Blueprint} from a YAML format API Blueprint AST file.
 *
 * @author pcornish
 * @see AbstractJsonAstParserService
 */
public abstract class AbstractYamlAstParserService<T extends Blueprint> extends AbstractAstParserService<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractYamlAstParserService.class);

    private Yaml yaml;

    public AbstractYamlAstParserService() {
        yaml = new Yaml();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T fromAst(InputStream astStream, AstFormat inputFormat) {
        try {
            if (!AstFormat.YAML.equals(inputFormat)) {
                throw new UnsupportedOperationException("This implementation only supports YAML format AST parsing");
            }

            LOGGER.info("Parsing {} format AST from stream", inputFormat);
            return yaml.loadAs(new InputStreamReader(astStream), getTargetModelClass());

        } catch (Exception e) {
            throw new ParserException(String.format("Error parsing %s format AST from stream", inputFormat), e);

        } finally {
            IOUtils.closeQuietly(astStream);
        }
    }
}
