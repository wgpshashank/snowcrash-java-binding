package com.gatehill.apib.parser.model.ast.v1;

/**
 * @author pcornish
 */
public abstract class AstBase {
    /**
     * Name
     */
    private String name;

    /**
     * Description
     */
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
