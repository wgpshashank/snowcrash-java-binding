package com.gatehill.apib.parser.model.ast.v2;

import java.util.List;

/**
 * @author pcornish
 */
public class AstParameter extends AstBase {
    /**
     * Type
     */
    private String type;

    /**
     * Required flag
     */
    private AstParameterUse use;

    /**
     * Default Value, applicable only when `required == false`
     */
    private String defaultValue;

    /**
     * Example Value
     */
    private String exampleValue;

    /**
     * Enumeration of possible values
     */
    private List<String> values;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public AstParameterUse getUse() {
        return use;
    }

    public void setUse(AstParameterUse use) {
        this.use = use;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getExampleValue() {
        return exampleValue;
    }

    public void setExampleValue(String exampleValue) {
        this.exampleValue = exampleValue;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

}
