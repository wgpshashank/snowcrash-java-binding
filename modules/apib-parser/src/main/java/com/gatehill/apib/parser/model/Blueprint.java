package com.gatehill.apib.parser.model;

/**
 * Created by pete on 13/10/2014.
 * <p/>
 * Common structure for version agnostic API Blueprints.
 *
 * @author pete
 */
public interface Blueprint {
    String getName();

    String getDescription();

    String get_version();
}
