package com.gatehill.apib.parser.model.ast.v1;

import java.util.List;
import java.util.Map;

/**
 * @author pcornish
 */
public class AstAction extends AstBase {
    /**
     * HTTP method
     */
    private String method;

    /**
     * Action-specfic Parameters
     */
    private Map<String, AstParameter> parameters;

    /**
     * Action-specific HTTP headers
     */
    private Map<String, AstValueType> headers;

    /**
     * Transactions examples
     */
    private List<AstTransactionExample> examples;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Map<String, AstParameter> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, AstParameter> parameters) {
        this.parameters = parameters;
    }

    public Map<String, AstValueType> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, AstValueType> headers) {
        this.headers = headers;
    }

    public List<AstTransactionExample> getExamples() {
        return examples;
    }

    public void setExamples(List<AstTransactionExample> examples) {
        this.examples = examples;
    }
}
