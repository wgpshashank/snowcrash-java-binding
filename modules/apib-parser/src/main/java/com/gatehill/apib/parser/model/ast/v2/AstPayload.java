package com.gatehill.apib.parser.model.ast.v2;

import java.util.List;
import java.util.Map;

/**
 * @author pcornish
 */
public abstract class AstPayload extends AstBase {
    /**
     * Payload-specific Parameters
     */
    private Map<String, AstParameter> parameters;

    /**
     * Payload-specific Headers
     */
    private List<AstNameValueType> headers;

    /**
     * Body
     */
    private String body;

    /**
     * Schema
     */
    private String schema;

    public List<AstNameValueType> getHeaders() {
        return headers;
    }

    public void setHeaders(List<AstNameValueType> headers) {
        this.headers = headers;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public Map<String, AstParameter> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, AstParameter> parameters) {
        this.parameters = parameters;
    }
}
