package com.gatehill.apib.parser.service;

import com.gatehill.apib.parser.model.AstFormat;
import com.gatehill.apib.parser.model.ParserConfiguration;
import com.gatehill.apib.parser.model.ParsingResult;

import java.io.File;
import java.io.InputStream;

/**
 * Converts an API Blueprint Markdown file into an AST, with a given format.
 *
 * @author pcornish
 */
public interface BlueprintParserService {
    /**
     * Convert the API Blueprint Markdown file into an AST {@link File}, with a given format.
     *
     * @param config
     * @param blueprintFile
     * @param outputFormat
     * @return
     */
    ParsingResult<File> convertToAstFile(ParserConfiguration config, File blueprintFile, AstFormat outputFormat);

    /**
     * Convert the API Blueprint Markdown file into an AST {@link String}, with a given format.
     *
     * @param config
     * @param blueprintFile
     * @param outputFormat
     * @return
     */
    ParsingResult<String> convertToAstString(ParserConfiguration config, File blueprintFile, AstFormat outputFormat);

    /**
     * Convert the API Blueprint Markdown file into an AST {@link InputStream}, with a given format.
     *
     * @param config
     * @param blueprintFile
     * @param outputFormat
     * @return
     */
    ParsingResult<InputStream> convertToAstStream(ParserConfiguration config, File blueprintFile, AstFormat outputFormat);
}
