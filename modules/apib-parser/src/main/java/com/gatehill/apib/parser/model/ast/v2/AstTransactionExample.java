package com.gatehill.apib.parser.model.ast.v2;

import java.util.List;

/**
 * @author pcornish
 */
public class AstTransactionExample extends AstBase {
    /**
     * Requests
     */
    private List<AstRequest> requests;

    /**
     * Responses
     */
    private List<AstResponse> responses;

    public List<AstRequest> getRequests() {
        return requests;
    }

    public void setRequests(List<AstRequest> requests) {
        this.requests = requests;
    }

    public List<AstResponse> getResponses() {
        return responses;
    }

    public void setResponses(List<AstResponse> responses) {
        this.responses = responses;
    }
}
