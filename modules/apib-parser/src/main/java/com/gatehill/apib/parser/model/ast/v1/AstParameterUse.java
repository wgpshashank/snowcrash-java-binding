package com.gatehill.apib.parser.model.ast.v1;

/**
 * @author pcornish
 */
public enum AstParameterUse {
    UndefinedParameterUse,
    OptionalParameterUse,
    RequiredParameterUse
}
