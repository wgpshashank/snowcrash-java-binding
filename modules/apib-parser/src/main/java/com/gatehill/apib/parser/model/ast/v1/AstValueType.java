package com.gatehill.apib.parser.model.ast.v1;

/**
 * @author pcornish
 */
public class AstValueType {
    /**
     * Value
     */
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
