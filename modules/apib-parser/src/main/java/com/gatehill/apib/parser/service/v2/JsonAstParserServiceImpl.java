package com.gatehill.apib.parser.service.v2;

import com.gatehill.apib.parser.model.ast.v2.AstBlueprint;
import com.gatehill.apib.parser.service.AbstractJsonAstParserService;

/**
 * Build a V1 Blueprint {@link com.gatehill.apib.parser.model.ast.v1.AstBlueprint} from a JSON format API
 * Blueprint AST file.
 *
 * @author pete
 * @see com.gatehill.apib.parser.service.v2.YamlAstParserServiceImpl
 */
public class JsonAstParserServiceImpl extends AbstractJsonAstParserService<AstBlueprint> {
    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<AstBlueprint> getTargetModelClass() {
        return AstBlueprint.class;
    }
}
