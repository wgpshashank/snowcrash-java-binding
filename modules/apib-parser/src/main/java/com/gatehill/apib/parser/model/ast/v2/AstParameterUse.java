package com.gatehill.apib.parser.model.ast.v2;

/**
 * @author pcornish
 */
public enum AstParameterUse {
    UndefinedParameterUse,
    OptionalParameterUse,
    RequiredParameterUse
}
