package com.gatehill.apib.parser.model.ast.v2;

/**
 * @author pcornish
 */
public class AstNameValueType {

    /**
     * Name
     */
    private String name;

    /**
     * Value
     */
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
