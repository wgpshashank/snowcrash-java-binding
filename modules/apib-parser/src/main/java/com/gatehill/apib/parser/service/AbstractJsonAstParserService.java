package com.gatehill.apib.parser.service;

import com.gatehill.apib.parser.exception.ParserException;
import com.gatehill.apib.parser.model.AstFormat;
import com.gatehill.apib.parser.model.Blueprint;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Build a {@link com.gatehill.apib.parser.model.Blueprint} from a JSON format API Blueprint AST file.
 *
 * @author pcornish
 * @see AbstractYamlAstParserService
 */
public abstract class AbstractJsonAstParserService<T extends Blueprint> extends AbstractAstParserService<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractJsonAstParserService.class);

    private Gson gson;

    public AbstractJsonAstParserService() {
        gson = new Gson();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T fromAst(InputStream astStream, AstFormat inputFormat) {
        try {
            if (!AstFormat.JSON.equals(inputFormat)) {
                throw new UnsupportedOperationException("This implementation only supports JSON format AST parsing");
            }

            LOGGER.info("Parsing {} format AST from stream", inputFormat);
            return gson.fromJson(new InputStreamReader(astStream), getTargetModelClass());

        } catch (Exception e) {
            throw new ParserException(String.format("Error parsing %s format AST from stream", inputFormat), e);

        } finally {
            IOUtils.closeQuietly(astStream);
        }
    }
}
