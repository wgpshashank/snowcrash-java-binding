package com.gatehill.apib.parser.service;

import com.gatehill.apib.parser.model.AstFormat;
import com.gatehill.apib.parser.model.Blueprint;

import java.io.File;
import java.io.InputStream;

/**
 * Loads an API Blueprint from an AST, in a given format.
 *
 * @author pcornish
 */
public interface AstParserService<T extends Blueprint> {
    /**
     * Load an API Blueprint from the AST {@link File}, in a given format.
     *
     * @param astFile
     * @param inputFormat
     * @return
     */
    T fromAst(File astFile, AstFormat inputFormat);

    /**
     * Load an API Blueprint from the AST {@link String}, in a given format.
     *
     * @param ast
     * @param inputFormat
     * @return
     */
    T fromAst(String ast, AstFormat inputFormat);

    /**
     * Load an API Blueprint from the AST {@link InputStream}, in a given format.
     *
     * @param astStream
     * @param inputFormat
     * @return
     */
    T fromAst(InputStream astStream, AstFormat inputFormat);
}
