package com.gatehill.apib.parser.exception;

/**
 * @author pcornish
 */
public class ParserException extends RuntimeException {
    public ParserException(String message) {
        super(message);
    }

    public ParserException(String message, Exception e) {
        super(message, e);
    }
}
