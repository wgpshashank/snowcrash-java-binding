package com.gatehill.apib.parser.service;

import com.gatehill.apib.parser.model.AstFormat;
import com.gatehill.apib.parser.model.ParserConfiguration;
import com.gatehill.apib.parser.model.ParsingResult;
import com.gatehill.apib.parser.model.result.AstResult;
import com.gatehill.apib.parser.support.TestUtil;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Tests for {@link SnowcrashBlueprintParserServiceImpl}.
 *
 * @author pcornish
 */
public class SnowcrashBlueprintParserServiceTest {
    private BlueprintParserService parser;
    private ParserConfiguration config;

    @Before
    public void before() {
        config = TestUtil.buildConfigFromEnvironmentVariable(TestUtil.ENV_SNOWCRASH2_HOME);

        parser = new SnowcrashBlueprintParserServiceImpl();
    }

    @Test
    public void testConvertToJsonFile() {
        // test data
        final File blueprintFile = new File(SnowcrashBlueprintParserServiceTest.class.getResource("/api1.md").getPath());

        // call
        final ParsingResult<File> actual = parser.convertToAstFile(config, blueprintFile, AstFormat.JSON);

        // assert output
        Assert.assertNotNull(actual);

        final AstResult result = actual.getResult();
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getError());
        Assert.assertEquals(AstResult.ErrorCode.NoError.getErrorCode(), result.getError().getCode());
        Assert.assertNotNull(result.getWarnings());
        Assert.assertEquals(0, result.getWarnings().length);

        final File astFile = actual.getAst();
        Assert.assertNotNull(astFile);
        Assert.assertTrue(astFile.getAbsolutePath().endsWith(".json"));
        Assert.assertTrue("AST file exists", astFile.exists());
    }

    @Test
    public void testConvertToYamlFile() {
        // test data
        final File blueprintFile = new File(SnowcrashBlueprintParserServiceTest.class.getResource("/api1.md").getPath());

        // call
        final ParsingResult<File> actual = parser.convertToAstFile(config, blueprintFile, AstFormat.YAML);

        // assert output
        Assert.assertNotNull(actual);

        final AstResult result = actual.getResult();
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getError());
        Assert.assertEquals(AstResult.ErrorCode.NoError.getErrorCode(), result.getError().getCode());
        Assert.assertNotNull(result.getWarnings());
        Assert.assertEquals(0, result.getWarnings().length);

        final File astFile = actual.getAst();
        Assert.assertNotNull(astFile);
        Assert.assertTrue(astFile.getAbsolutePath().endsWith(".yaml"));
        Assert.assertTrue("AST file exists", astFile.exists());
    }

    @Test
    public void testConvertToJsonString() {
        // test data
        final File blueprintFile = new File(SnowcrashBlueprintParserServiceTest.class.getResource("/api1.md").getPath());

        // call
        final ParsingResult<String> actual = parser.convertToAstString(config, blueprintFile, AstFormat.JSON);

        // assert output
        Assert.assertNotNull(actual);

        final AstResult result = actual.getResult();
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getError());
        Assert.assertEquals(AstResult.ErrorCode.NoError.getErrorCode(), result.getError().getCode());
        Assert.assertNotNull(result.getWarnings());
        Assert.assertEquals(0, result.getWarnings().length);

        Assert.assertNotNull(actual.getAst());
    }

    @Test
    public void testConvertToYamlStream() throws IOException {
        // test data
        final File blueprintFile = new File(SnowcrashBlueprintParserServiceTest.class.getResource("/api1.md").getPath());

        // call
        final ParsingResult<InputStream> actual = parser.convertToAstStream(config, blueprintFile, AstFormat.YAML);

        // assert output
        Assert.assertNotNull(actual);

        final AstResult result = actual.getResult();
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getError());
        Assert.assertEquals(AstResult.ErrorCode.NoError.getErrorCode(), result.getError().getCode());
        Assert.assertNotNull(result.getWarnings());
        Assert.assertEquals(0, result.getWarnings().length);

        Assert.assertNotNull(actual.getAst());
        Assert.assertNotNull("Stream should have content", IOUtils.toString(actual.getAst()));
    }
}
