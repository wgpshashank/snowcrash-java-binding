package com.gatehill.apib.parser.service.v1;

import com.gatehill.apib.parser.exception.ParserException;
import com.gatehill.apib.parser.model.AstFormat;
import com.gatehill.apib.parser.model.ast.v1.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

/**
 * Tests for {@link JsonAstParserServiceImpl}.
 *
 * @author pcornish
 */
public class JsonAstParserServiceTest {

    private JsonAstParserServiceImpl service;

    @Before
    public void before() {
        service = new JsonAstParserServiceImpl();
    }

    @Test
    public void testFromAst_Success() throws Exception {
        // test data
        final File blueprint = new File(JsonAstParserServiceTest.class.getResource("/v1/api1.json").getPath());

        // call
        final AstBlueprint astBlueprint = service.fromAst(blueprint, AstFormat.JSON);

        // assert
        Assert.assertNotNull(astBlueprint);
        Assert.assertEquals("My API", astBlueprint.getName());
        Assert.assertEquals(1, astBlueprint.getResourceGroups().size());

        Assert.assertEquals(1, astBlueprint.getResourceGroups().get(0).getResources().size());
        final AstResource astResource = astBlueprint.getResourceGroups().get(0).getResources().get(0);
        Assert.assertNotNull(astResource);
        Assert.assertEquals("/message", astResource.getUriTemplate());

        Assert.assertEquals(1, astResource.getActions().size());
        final AstAction astAction = astResource.getActions().get(0);
        Assert.assertEquals("GET", astAction.getMethod());

        Assert.assertEquals(1, astAction.getExamples().size());
        final AstTransactionExample astExample = astResource.getActions().get(0).getExamples().get(0);
        Assert.assertEquals(0, astExample.getRequests().size());

        Assert.assertEquals(1, astExample.getResponses().size());
        final AstResponse astResponse = astExample.getResponses().get(0);
        Assert.assertEquals("Hello World!\n", astResponse.getBody());
        Assert.assertEquals("200", astResponse.getName());

        Assert.assertEquals(1, astResponse.getHeaders().size());
        final AstValueType header = astResponse.getHeaders().get("Content-Type");
        Assert.assertNotNull(header);
        Assert.assertEquals("text/plain", header.getValue());
    }

    @Test(expected = ParserException.class)
    public void testFromAst_UnsupportedFormat() throws Exception {
        // test data
        final File blueprint = new File(JsonAstParserServiceTest.class.getResource("/api1.md").getPath());

        // call
        service.fromAst(blueprint, AstFormat.YAML);

        // assert
        Assert.fail(UnsupportedOperationException.class + " should have been thrown");
    }
}
